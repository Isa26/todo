from django.shortcuts import get_object_or_404
from .models import ToDoTask
from .mailgun import send_mail_task


class NotificationEmail:

    def __init__(self, pk):
        self.pk = pk

    def send_notification(self):
        task = get_object_or_404(ToDoTask, pk=self.pk)
        to = [task.task_creator.email]
        subject = "To Do app notification"
        txt = "10 minutes to the end of the {}".format(task.title)
        send_mail_task(to, with_mailgun=True, subject=subject, message=txt)