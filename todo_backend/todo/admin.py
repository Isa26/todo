from django.contrib import admin
from .forms import BaseUserChangeForm, BaseUserCreationForm
from .admin_actions import *
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from .models import ToDoTask, ShareTask, TaskComment
# Register your models here.

UserModel = get_user_model()


@admin.register(UserModel)
class BaseUserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name',)}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ("first_name", "last_name", 'email', 'password1', 'password2'),
        }),
    )
    form = BaseUserChangeForm
    add_form = BaseUserCreationForm
    list_display = ('pk', 'email', 'first_name', 'last_name', 'is_active',)
    list_filter = ('is_active', 'is_staff', 'groups',)
    search_fields = ('first_name', 'last_name', 'email',)
    ordering = ('date_joined',)
    filter_horizontal = ('groups', 'user_permissions',)
    readonly_fields = ("date_joined",)

    actions = [make_publish_active, make_publish_unactive,]
    date_hierarchy = 'date_joined'


@admin.register(ToDoTask)
class ToDoTaskAdmin(admin.ModelAdmin):
    list_display = ('pk', 'title', 'task_creator_email', 'start_date', 'end_date', 'task_description',)
    search_fields = ('task_creator__email', 'title', 'description',)
    readonly_fields = ("created_at",)
    ordering = ("created_at",)

    def task_creator_email(self, obj):
        return obj.task_creator.email

    def task_description(self, obj):
        return obj.description[:100] + '...'


@admin.register(ShareTask)
class ShareTaskAdmin(admin.ModelAdmin):
    list_display = ('pk', 'get_user', 'get_task', 'read_and_comment', 'created_at',)
    search_fields = ('task__title', 'user__email',)
    readonly_fields = ("created_at",)

    def get_user(self, obj):
        return obj.user.email

    def get_task(self, obj):
        return obj.task.title

    get_task.short_description = _('Share task title')


@admin.register(TaskComment)
class TaskComment(admin.ModelAdmin):
    list_display = ("short_commet_text", "task_title", "comment_user_email", "created_at",)
    search_fields = ('comment_text', 'comment_user__email', 'task__title',)
    readonly_fields = ("created_at",)

    def short_commet_text(self, obj):
        return obj.comment_text[:100] + '...'

    def task_title(self, obj):
        return obj.task.title

    def comment_user_email(self, obj):
        return obj.comment_user.email

    short_commet_text.short_description = _('Short description')
    task_title.short_description = _('Task title')
    comment_user_email.short_description = _('User email')