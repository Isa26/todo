def time_difference(time_start, time_end):
    difference = time_end - time_start
    minutes = difference.total_seconds() / 60
    return int(minutes)