from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth import get_user_model
from .models import BaseUser


USER_MODEL = get_user_model()


@receiver(post_save, sender=BaseUser)
def handler_user_model_post_save(created, sender, update_fields, instance=None, **kwargs):
    def make_user_active(instance):
        if created:
            if instance.is_active == False:
                instance.is_active = True
                instance.save()

    make_user_active(instance)