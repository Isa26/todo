from django import forms
from django.contrib.auth import get_user_model, password_validation
from django.contrib.auth.forms import ReadOnlyPasswordHashField, AuthenticationForm
from django.utils.translation import ugettext_lazy as _
from .models import ToDoTask, ShareTask
User = get_user_model()


class BaseUserCreationForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given email and
    password.
    """
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }
    password1 = forms.CharField(label=_("Пороль"),
                                widget=forms.PasswordInput(attrs={'placeholder': 'Password'}))
    password2 = forms.CharField(label=_("Повторите пороль"),
                                widget=forms.PasswordInput(attrs={'placeholder': 'Re-enter Password'}),
                                help_text=_("Enter the same password as above, for verification."))

    class Meta:
        model = User
        fields = ("email", "first_name", "last_name")

    def __init__(self, *args, **kwargs):
        super(BaseUserCreationForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget = forms.TextInput(attrs={
            'placeholder': 'Email'})
        self.fields['first_name'].widget = forms.TextInput(attrs={
            'placeholder': 'First name'})
        self.fields['last_name'].widget = forms.TextInput(attrs={
            'placeholder': 'Last name'})

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        password_validation.validate_password(self.cleaned_data.get('password2'), self.instance)
        return password2

    def save(self, commit=True):
        user = super(BaseUserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class BaseUserChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField(label=_("Password"),
                                         help_text=_("Raw passwords are not stored, so there is no way to see "
                                                     "this user's password, but you can change the password "
                                                     "using <a href=\"../password/\">this form</a>."))

    class Meta:
        model = User
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(BaseUserChangeForm, self).__init__(*args, **kwargs)
        f = self.fields.get('user_permissions', None)
        if f is not None:
            f.queryset = f.queryset.select_related('content_type')

    def clean_password(self):
        return self.initial["password"]


class UserRegistrationForm(BaseUserCreationForm):
    accept = forms.BooleanField(initial=False,
                                label=_('I accept the terms of the user agreement.'),
                                error_messages={'required': _("You must confirm this rule.")})


class UserAuthenticationForm(AuthenticationForm):
    username = forms.CharField(max_length=254, label=_("Логин"))
    password = forms.CharField(label=_("Пароль"), widget=forms.PasswordInput())

    def __init__(self, *args, **kwargs):
        super(UserAuthenticationForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget = forms.TextInput(attrs={
            'placeholder': 'E-mail:'})
        self.fields['password'].widget = forms.PasswordInput(attrs={
            'placeholder': 'Password:'})


class CreateTaskForm(forms.ModelForm):
    error_messages = {
        'date_error': _("End date must be more than start date"),
    }

    class Meta:
        model = ToDoTask
        fields = ('title', 'description', 'start_date', 'end_date')

    def clean_end_date(self):
        start_date = self.cleaned_data.get('start_date')
        end_date = self.cleaned_data.get('end_date')
        if start_date is not None and end_date is not None:
            if start_date >= end_date:
                raise forms.ValidationError(
                    self.error_messages['date_error'],
                    code='date_error',
                )
        return end_date


class UpdateTaskForm(CreateTaskForm):

    start_date = forms.DateTimeField(input_formats=['%Y-%m-%d %H:%M:%S'])
    end_date = forms.DateTimeField(input_formats=['%Y-%m-%d %H:%M:%S'])

    class Meta:
        model = ToDoTask
        fields = ('title', 'description', 'start_date', 'end_date')


class SearchForm(forms.Form):
   user_name = forms.CharField(required=True)


class CommentForm(forms.Form):
   comment = forms.CharField(widget=forms.Textarea, required=True)


class ShareTaskForm(forms.ModelForm):

    def clean(self):
        share_users = self.cleaned_data.get('share_users')
        task = self.cleaned_data.get('task')
        if task:
            all_user_list = []
            creator = task.task_creator
            for user in share_users:
                all_user_list.append(user)
            if creator in all_user_list:
                raise forms.ValidationError(
                    'Task creator cannot be included in the task share user list'
                )
        # share_tasks = ShareTask.objects.filter(task=task)
        # task_list = []
        # for share in share_tasks:
        #     task_list.append(share.task)
        # if task in task_list:
        #     raise forms.ValidationError('ShareTask with this task has already been created.')
        return self.cleaned_data





