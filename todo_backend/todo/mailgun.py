import requests
from django.template import loader
from django.core.mail import EmailMessage
from django.conf import settings


def send_mail_task(user_emails, with_mailgun=False, subject="Fibonacci", template_name='mail.html', **kwargs):
    if 'message' not in kwargs:
        raise ValueError('Message not in parameters')

    mail_html = loader.get_template(template_name).render(kwargs)
    print("send mail")

    if with_mailgun:
        requests.post(
            "https://api.mailgun.net/v3/mail-server.safaroff.com/messages",
            auth=("api", "key-b6f424497aeaae7bc768fc07243b2f4b"),
            data={"from": "ToDo contact mail <info@todo.ru>",
                  "to": user_emails,
                  "subject": subject,
                  "text": kwargs['message'],
                  "html": mail_html})
    else:
        try:
            subject = subject
            to = user_emails
            from_email = settings.EMAIL_HOST_USER
            msg = EmailMessage(subject, mail_html, to=to, from_email=from_email)
            msg.content_subtype = 'html'
            msg.send()
        except Exception as e:
            pass