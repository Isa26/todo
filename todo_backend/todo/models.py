from django.db import models
from .managers import CustomUserManager
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.core.exceptions import ValidationError
# Create your models here.

USER_MODEL = settings.AUTH_USER_MODEL


class BaseUser(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(verbose_name=_('Fist name'), max_length=255, null=True, blank=True)
    last_name = models.CharField(verbose_name=_('Last name'), max_length=255, null=True, blank=True)
    email = models.EmailField(_('e-mail'), max_length=255, unique=True)
    is_staff = models.BooleanField(_('active'), default=False,
                                   help_text='Designates whether the user can log into this admin site.')
    is_active = models.BooleanField(_('active_status'), default=False,
                                    help_text=('Designates whether this user should be treated as '
                                               'active. Unselect this instead of deleting accounts.'))

    date_joined = models.DateTimeField(_('date_joined'), auto_now_add=True)

    # activation confirm

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '{} {}'.format(self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """
        returns short name for the User
        """
        return self.email

    def __str__(self):
        return str(self.email)


class ToDoTask(models.Model):
    title = models.CharField(verbose_name=_("Task title"), max_length=650)
    description = models.TextField(verbose_name=_("Task description"))
    start_date = models.DateTimeField(verbose_name=_("Task start date"))
    end_date = models.DateTimeField(verbose_name=_("Task end date"))
    task_creator = models.ForeignKey(USER_MODEL, verbose_name=_("Task creator"))
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    def clean(self):
        if self.end_date and self.start_date:
            if not self.end_date > self.start_date:
                raise ValidationError("End date must be more than start date")
        return self


class ShareTask(models.Model):
    user = models.ForeignKey(USER_MODEL, verbose_name=_("Share Users"))
    task = models.ForeignKey(ToDoTask, verbose_name=_("Task"))
    read_and_comment = models.BooleanField(default=False, verbose_name=_('Read and Comment?'))
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.task.title


class TaskComment(models.Model):
    comment_text = models.TextField(verbose_name=_("Comment text"))
    task = models.ForeignKey(ToDoTask, verbose_name=_("Task related with comment"))
    comment_user = models.ForeignKey(USER_MODEL, verbose_name=_("User related with comment"))
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.task.title
