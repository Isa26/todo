from rest_framework import viewsets
from ..models import ToDoTask
from .serializers import ToDoTaskSerializer
from rest_framework.permissions import IsAuthenticated


class ToDoViewSet(viewsets.ModelViewSet):
    serializer_class = ToDoTaskSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return ToDoTask.objects.filter(task_creator=self.request.user)

    def perform_create(self, serializer):
        serializer.save(task_creator=self.request.user)
