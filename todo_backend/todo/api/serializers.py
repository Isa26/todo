from rest_framework import serializers
from ..models import ToDoTask


class ToDoTaskSerializer(serializers.ModelSerializer):

    url = serializers.HyperlinkedIdentityField(view_name='to_do:todo_api-detail',)

    # def validate_task_creator(self, value):
    #     user = self.context['request'].user
    #     return user

    def validate(self, data):
        if data['start_date'] > data['end_date']:
            raise serializers.ValidationError("End date must be more than start date")
        return data

    class Meta:
        model = ToDoTask
        fields = ('pk', 'url', 'title', 'task_creator', 'description', 'start_date', 'end_date',)
        read_only_fields = ('task_creator',)

