from django.conf.urls import url
from .views import ToDoViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()

router.register(r'todo', ToDoViewSet, base_name='todo_api')