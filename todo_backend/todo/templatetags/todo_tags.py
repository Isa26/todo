from django.shortcuts import get_object_or_404
from django import template
from django.contrib.auth import get_user_model
from ..models import ShareTask, ToDoTask
register = template.Library()

User = get_user_model()


@register.filter()
def get_user_attend(pk, task_pk):
    user = get_object_or_404(User, pk=pk)
    task = get_object_or_404(ToDoTask, pk=task_pk)
    share_task = ShareTask.objects.filter(task=task, user=user).last()
    is_attend = False
    if share_task:
        is_attend = True
    else:
        is_attend = False
    return is_attend



