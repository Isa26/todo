from django.apps import AppConfig


class TodoAppConfig(AppConfig):
    name = 'todo'

    def ready(self):
        import todo.signals
        import todo.tasks
