from django.utils.translation import ugettext_lazy as _


def make_publish_active(modeladmin, request, queryset):
    queryset.update(is_active=True)


def make_publish_unactive(modeladmin, request, queryset):
    queryset.update(is_active=False)


make_publish_active.short_description = _("Mark selected items are active")
make_publish_unactive.short_description = _("Mark selected items are unactive")