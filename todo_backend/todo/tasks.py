from celery.task import PeriodicTask
from .tools import NotificationEmail
from .models import ToDoTask
from .time_diff import time_difference
from django.utils import timezone
from datetime import timedelta


class SendNotificationTask(PeriodicTask):
    run_every = timedelta(minutes=1)

    def run(self, *args, **kwargs):
        self.send_note()

    def send_note(self):
        tasks = ToDoTask.objects.all()
        for task in tasks:
            now = timezone.now()
            end_date = task.end_date
            if time_difference(now, end_date) == 10:
                notification = NotificationEmail(task.pk)
                notification.send_notification()



