from django.conf.urls import url
from . import views
from .views import *
from .api.routers import router


urlpatterns = [
    url(r'^registration/$', UserRegistrationView.as_view(), name='registration'),
    url(r'^login/$', UserLoginView.as_view(), name='login'),
    url(r'^logout/$', UserLogoutView.as_view(), name='logout'),
    url(r'^$', MainPageView.as_view(), name='main'),
    url(r'^create/$', CreateToDoView.as_view(), name='create'),
    url(r'^list/$', ToDoListView.as_view(), name='list'),
    url(r'^update/(?P<pk>\d+)/$', UpdateToDoView.as_view(), name='update'),
    url(r'^delete/(?P<pk>\d+)/$', DeleteToDoView.as_view(), name='delete'),
    url(r'^deleted/$', DeletedView.as_view(), name='deleted'),
    url(r'^share/(?P<pk>\d+)/$', ShareTaskView.as_view(), name='share'),
    url(r'^task/(?P<pk>\d+)/$', TaskDetailView.as_view(), name='task'),
    url(r'^shared_tasks/$', SharedTasksListView.as_view(), name='shared_tasks'),
]

urlpatterns += router.urls