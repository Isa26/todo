from django.shortcuts import render, redirect, get_object_or_404
import json
from django.views.generic import *
from django.http import HttpResponseRedirect, JsonResponse, Http404
from .forms import UserRegistrationForm, UserAuthenticationForm, CreateTaskForm, UpdateTaskForm, SearchForm, CommentForm
from django.urls import reverse, reverse_lazy
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import authenticate, login, logout
from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import Q
from .models import *
import logging
log = logging.getLogger(__name__)

User = get_user_model()


class UserRegistrationView(CreateView):
    model = User
    form_class = UserRegistrationForm
    template_name = 'registration.html'
    main_page = 'to_do:main'
    context_object_name = 'form'

    def get(self, request):
        context = {}
        if request.user.is_authenticated():
            return redirect(self.main_page)
        context['form'] = self.form_class
        return render(request, self.template_name, context=context)

    def form_valid(self, form):
        form.save()
        username = form.cleaned_data['email']
        password = form.cleaned_data['password1']
        new_user = authenticate(username=username, password=password)
        login(self.request, new_user, backend='django.contrib.auth.backends.ModelBackend')
        return redirect(self.main_page)


class UserLoginView(FormView):
    form_class = UserAuthenticationForm
    template_name = 'login.html'
    success_url = reverse_lazy('to_do:main')
    home_page = reverse_lazy('to_do:main')
    logout_url = reverse_lazy('to_do:logout')

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return redirect(self.home_page)
        return super(UserLoginView, self).get(request, *args, **kwargs)

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            if next_url == self.logout_url:
                return self.home_page
            return next_url
        else:
            return self.home_page

    def form_valid(self, form):
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']

        user = authenticate(username=username, password=password)
        login(self.request, user)
        if user.is_active:
            login(self.request, user)
            return redirect(self.success_url)
        else:
            return self.form_invalid(form)


class UserLogoutView(LoginRequiredMixin, RedirectView):
    url = reverse_lazy('to_do:login')
    login_url = reverse_lazy('to_do:login')

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(UserLogoutView, self).get(request, *args, **kwargs)


class MainPageView(TemplateView):
    template_name = 'main.html'


class CreateToDoView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    template_name = 'todo_create.html'
    form_class = CreateTaskForm
    login_url = reverse_lazy('to_do:login')
    success_url = reverse_lazy('to_do:create')
    success_message = 'Task created successfully'

    def get(self, request):
        return render(request, self.template_name, {'form': self.form_class})

    def get_success_message(self, cleaned_data):
        return self.success_message

    def form_valid(self, form):
        task = form.save(commit=False)
        task.task_creator = self.request.user
        task.save()
        return super(CreateToDoView, self).form_valid(form)


class ToDoListView(LoginRequiredMixin, ListView):
    model = ToDoTask
    template_name = 'todo_list.html'
    login_page = 'to_do:login'
    login_url = reverse_lazy('to_do:login')

    def get_queryset(self):
        return self.model.objects.filter(task_creator=self.request.user)

    def get(self, request):
        return render(request, self.template_name, {'todo_list': self.get_queryset()})


class UpdateToDoView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = ToDoTask
    template_name = 'todo_update.html'
    form_class = UpdateTaskForm
    login_url = reverse_lazy('to_do:login')
    success_message = 'Task updated successfully'

    def get_object(self, queryset=None):
        return get_object_or_404(ToDoTask, pk=self.kwargs['pk'])

    def get_success_url(self):
        return reverse('to_do:update', args=(self.get_object().pk,))

    def get(self, request, pk):
        context = {}
        context['todo'] = self.get_object()
        context['form'] = self.form_class
        if not request.user.is_authenticated():
            return redirect(self.login_page)
        return render(request, self.template_name, context=context)

    def get_success_message(self, cleaned_data):
        return self.success_message

    def form_valid(self, form):
        task = form.save(commit=False)
        task.task_creator = self.request.user
        task.save()
        return super(UpdateToDoView, self).form_valid(form)


class DeleteToDoView(LoginRequiredMixin, DeleteView):
    model = ToDoTask
    template_name = 'todo_delete.html'
    login_url = reverse_lazy('to_do:login')
    success_url = reverse_lazy('to_do:deleted')
    success_message = 'Task deleted successfully'

    def get_object(self, queryset=None):
        return get_object_or_404(ToDoTask, pk=self.kwargs['pk'])

    def get(self, request, pk):
        context = {}
        context['todo'] = self.get_object()
        return render(request, self.template_name, context=context)


class DeletedView(TemplateView):
    template_name = 'deleted.html'


class ShareTaskView(LoginRequiredMixin, View):
    template_name = 'share_task.html'
    form_class = SearchForm
    login_url = reverse_lazy('to_do:login')

    def get_todo(self, pk):
        return get_object_or_404(ToDoTask, pk=pk)

    def get(self, request, pk):
        context = {}
        context['form'] = self.form_class
        context['todo'] = self.get_todo(pk)
        return render(request, self.template_name, {'form': self.form_class})

    def post(self, request, pk):
        context = {}
        return_dict = dict()
        data = request.POST
        method = data.get('method')
        if method:
            if method == 'share':
                user_id = data.get('user_id')
                status = data.get('status')
                log.debug(type(status))
                log.debug(data)
                log.debug(type(data))
                user = get_object_or_404(User, pk=user_id)
                task = self.get_todo(pk)
                share = ShareTask.objects.filter(task=task, user=user).last()
                is_attend = 0
                read_comment = False
                if status == 'true':
                    read_comment = True
                if share:
                    share.delete()
                    is_attend = 0
                else:
                    ShareTask.objects.create(task=task, user=user, read_and_comment=read_comment)
                    is_attend = 1
                return_dict['comment'] = read_comment
                return_dict['status'] = status
                return_dict['is_attend'] = is_attend
                return JsonResponse(return_dict, status=200)
        else:
            form = self.form_class(request.POST)
            if form.is_valid():
                user_name = form.cleaned_data['user_name']
                users = User.objects.filter(Q(first_name__icontains=user_name) |
                                Q(email__icontains=user_name), is_superuser=False).exclude(pk=self.request.user.pk)
                context['users'] = users
            else:
                context['errors'] = form.errors
        context['form'] = self.form_class(request.POST)
        context['result'] = self.request.POST.get('user_name')
        context['todo'] = self.get_todo(pk)
        return render(request, self.template_name, context)


class TaskDetailView(LoginRequiredMixin, View):
    model = ToDoTask
    template_name = 'todo_detail.html'
    form_class = CommentForm
    login_url = reverse_lazy('to_do:login')
    home_page = 'to_do:main'

    def get_task(self, pk):
        task = get_object_or_404(self.model, pk=pk)
        if task.task_creator != self.request.user:
            share_task = self.request.user.sharetask_set.filter(task__pk=pk).last()
            if share_task:
                return share_task.task
            else:
                raise Http404
        else:
            return task

    def get_comments(self, pk):
        task = self.get_task(pk)
        comments = TaskComment.objects.filter(task=task).order_by('created_at')
        return comments

    def let_comments(self, pk):
        is_show_comments = True
        task = get_object_or_404(self.model, pk=pk)
        if task.task_creator != self.request.user:
            share_task = self.request.user.sharetask_set.filter(task__pk=pk).last()
            if share_task:
                if not share_task.read_and_comment:
                    is_show_comments = False
        return is_show_comments

    def get(self, request, pk):
        context = {}
        context['is_show'] = self.let_comments(pk)
        context['task'] = self.get_task(pk)
        context['form'] = self.form_class()
        context['comments'] = self.get_comments(pk)
        return render(request, self.template_name, context)

    def post(self, request, pk):
        form = self.form_class(request.POST)
        task = self.get_task(pk)
        context = {}
        if form.is_valid():
            comment = form.cleaned_data['comment']
            comment_user = request.user
            TaskComment.objects.create(comment_text=comment, task=task, comment_user=comment_user)
        else:
            context['errors'] = form.errors
        context['form'] = self.form_class(request.POST)
        context['task'] = task
        context['comments'] = self.get_comments(pk)
        return render(request, self.template_name, context)


class SharedTasksListView(LoginRequiredMixin, View):
    template_name = 'shared_tasks.html'
    login_url = reverse_lazy('to_do:login')

    def shared_tasks(self):
        shared_tasks = self.request.user.sharetask_set.all()
        tasks = []
        for task in shared_tasks:
            tasks.append(task.task)
        return tasks

    def get(self, request):
        tasks = self.shared_tasks()
        context = {}
        context['tasks'] = tasks
        return render(request, self.template_name, context)




