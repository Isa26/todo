from fabric.api import run
from fabric.operations import local as lrun, run
from fabric.api import task
from fabric.state import env


def restart():
    lrun('sudo systemctl restart todo_backend-gunicorn.service')
    lrun('sudo systemctl restart nginx')
    lrun('sudo systemctl restart redis')