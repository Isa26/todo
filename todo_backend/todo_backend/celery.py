from __future__ import absolute_import

import os

from celery import Celery

from django.conf import settings

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'todo_backend.settings')

app = Celery('todo_backend',
             broker='redis://localhost:6379/1',
             backend='redis://localhost:6379/1')

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

app.accept_content = ['pickle', 'json', 'msgpack', 'yaml']


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
