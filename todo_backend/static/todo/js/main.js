$(document).ready(function() {

    $(".share").click(function(e){


        e.preventDefault();
        var user_id = $(this).data('user_id');
        var status = false;
        var url = window.location.href;
        var share = $(this);
        var share_block = $(this).closest('.share_block');
        var share_box = share_block.find('input:checkbox:first');
        if (share_box.prop('checked') == true){
            status = true;
        }
        var data = {'user_id': user_id, 'status': status, 'method': 'share'};
        console.log(data);
        $.ajax({
            url: url,
            data: data,
            type: 'POST',
            success: function(data){
                var is_attend = data.is_attend
                console.log(data.status)
                console.log(data.comment)
                if (is_attend == 1){
                    $(share).text('Remove from share list');
                }else{
                    $(share).text('Add to share list');
                }


            },
            error: function(error){
                console.log(error);
            }

        });

        return false;
    });


});